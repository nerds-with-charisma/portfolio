<!-- MMMMMMMMMM that's good Readme -->
<p align="center">
  <a href="https://nerdswithcharisma.com">
    <img alt="NWC" src="https://d33wubrfki0l68.cloudfront.net/7380a5b5b8a8d475e1dc4fe2d7c2e869ae89991c/943bf/404/logo--nwc-purple.svg" width="90" />
  </a>
</p>
<h1 align="center">
  Nerds With Charisma
</h1>

This is the NWC Portfolio source code. Feel free to poke around. This site was build with Gatsby, a static-site generator that is built upon React.

_Never heard of Gatsby? Check out their [official docs here](https://www.gatsbyjs.org/docs/)._

\_Not familiar with React? Try Traversy Media's [React Course](https://www.youtube.com/watch?v=sBws8MSXN7A&t=6s) on Youtube. Or [AceMind's course on React Hooks](https://www.youtube.com/watch?v=-MlNBTSg_Ww).

## 🖖 Commands

• **Start developing locally.**

Navigate into your new site’s directory and start it up with the develop command.

```sh
  cd <project directory>
  gatsby develop
```

This will spin up the local server running on `http://localhost:8000`. Any changes you make will be instantly reflected.

_Note: Although we're not using it on this website, you also get GraphQl Support @ _`http://localhost:8000/___graphql`\_.

Build the production static site, output to the public directory:

```sh
  gatsby build
```

Lint everything (basd off airb&b linting)

```sh
  npm run lint
```

## 🔬 Testing

**Static Testing**
Eslint will run automatically with the develop command. You may modify .eslintrc to fit your needs.

Husky is installed along with lint-staged to test your code before committing to git to make sure it is lint'd and error free before making it to your repo. [Learn More](https://www.youtube.com/watch?v=BzRAYt7BHRw)

**End to End Testing**
Cypress lets you test visually elements and functionality are working correctly in a mini-chrome browser. Specify tests in smoke.js or create new files in cypress/e2e

`npm run cy:open`

**Unit Testing**
Jest is used to run unit tests to verify individual and specific functionality works as expected.

## 🍺 What's inside?

A quick look at the top-level files and directories you'll see in a Gatsby project.

    .
    ├── node_modules
    ├── src
    ├── .gitignore
    ├── .prettierrc
    ├── gatsby-browser.js
    ├── gatsby-config.js
    ├── gatsby-node.js
    ├── gatsby-ssr.js
    ├── LICENSE
    ├── package-lock.json
    ├── package.json
    └── README.md

1.  **`/node_modules`**: This directory contains all of the modules of code that your project depends on (npm packages) are automatically installed.

2.  **`/src`**: This directory will contain all of the code related to what you will see on the front-end of your site (what you see in the browser) such as your site header or a page template. `src` is a convention for “source code”.

3.  **`.gitignore`**: This file tells git which files it should not track / not maintain a version history for.

4.  **`.prettierrc`**: This is a configuration file for [Prettier](https://prettier.io/). Prettier is a tool to help keep the formatting of your code consistent.

5.  **`gatsby-browser.js`**: This file is where Gatsby expects to find any usage of the [Gatsby browser APIs](https://www.gatsbyjs.org/docs/browser-apis/) (if any). These allow customization/extension of default Gatsby settings affecting the browser.

6.  **`gatsby-config.js`**: This is the main configuration file for a Gatsby site. This is where you can specify information about your site (metadata) like the site title and description, which Gatsby plugins you’d like to include, etc. (Check out the [config docs](https://www.gatsbyjs.org/docs/gatsby-config/) for more detail).

7.  **`gatsby-node.js`**: This file is where Gatsby expects to find any usage of the [Gatsby Node APIs](https://www.gatsbyjs.org/docs/node-apis/) (if any). These allow customization/extension of default Gatsby settings affecting pieces of the site build process.

8.  **`gatsby-ssr.js`**: This file is where Gatsby expects to find any usage of the [Gatsby server-side rendering APIs](https://www.gatsbyjs.org/docs/ssr-apis/) (if any). These allow customization of default Gatsby settings affecting server-side rendering.

9.  **`LICENSE`**: Gatsby is licensed under the MIT license.

10. **`package-lock.json`** (See `package.json` below, first). This is an automatically generated file based on the exact versions of your npm dependencies that were installed for your project. **(You won’t change this file directly).**

11. **`package.json`**: A manifest file for Node.js projects, which includes things like metadata (the project’s name, author, etc). This manifest is how npm knows which packages to install for your project.

12. **`README.md`**: A text file containing useful reference information about your project.

## 🎓 Learning Gatsby

Looking for more guidance? Full documentation for Gatsby lives [on the website](https://www.gatsbyjs.org/). Here are some places to start:

- **For most developers, we recommend starting with our [in-depth tutorial for creating a site with Gatsby](https://www.gatsbyjs.org/tutorial/).** It starts with zero assumptions about your level of ability and walks through every step of the process.

- **To dive straight into code samples, head [to our documentation](https://www.gatsbyjs.org/docs/).** In particular, check out the _Guides_, _API Reference_, and _Advanced Tutorials_ sections in the sidebar.

## 🐵 USEFUL LINKS

[Nerds With Charisma](https://nerdswithcharisma.com)

[Gatsby's Docs](https://www.gatsbyjs.org/docs/)

[Traversy Media YouTube Videon on React](https://www.youtube.com/watch?v=sBws8MSXN7A)

[Acemind YouTube Video on React Hooks](https://www.youtube.com/watch?v=-MlNBTSg_Ww)

[Emoji List](https://unicode.org/emoji/charts/full-emoji-list.html)

## Helpful Git Commands

Init Git repo, if you don't already have one

```sh
git init
```

checkout a remote (from Gitlab or Github)

```sh
  git remote add <source> <repo path>

  example:
  git remote add origin https://some-git-repo
```

Stage all your changes to prep to push to git.

```sh
git add .
```

Check the status of your changes

```sh
git status
```

Push the changes to your git repo

```sh
git commit -m "My awesome message"
git push
```

Create a new branch

```sh
git branch <branch name>
```

See all branches

```sh
git branch -a
```

Merge branch to master

```sh
git checkout master
git merge <branch name>
```

<!--
Make the title of each page funnier

√ # Intro
Add "big thanks" to commonly referenced things
(
  https://www.udemy.com/user/brad-traversy/
  https://wesbos.com/courses/

)

√ # Front end 101
    - IDE
      √ Talk about different IDEs, Choose one, basic terminal commands
      √ Go through setup for VSCode

    - Browser
      √  Show how to open the page in browser, talk about debugging 101

    - HTML & CSS Basics
      Show required reading, build something super simple (weather app layout?)

    - Building a simple coming soon page
      Build a simple coming soon page

    - Git

    - Hosting (Classic/FTP, Netlify, Heroku) & Domain

    - Add Google Analytics

    - Add resume (show how to link to files on the server)

√ # Javascript 101
  - Javascript basics

  - Books to read & videos

  - Pointless but cools stuff

√ # React and Gatsby <3
  - Getting started with React

  - Getting started with gatsby

  - Setup our basic meta data
    - lang, title, favicon, charSet, viewport, description, keywords, canonical

  - Sass 101

√ # Building Sections
  Header
  Hero
  Services
  About
  Footer

# SEO Part 1
   √ GTM & Google analytics
   √ 404 page
   √ Robots.txt  - http://tools.seobook.com/robots-txt/generator/
   √ sitemap - https://www.xml-sitemaps.com/
   Extra meta data
    √ - Favicon
    √ - Extra meta
    √ - Color address bar - https://www.wpbeginner.com/wp-tutorials/how-to-change-the-color-of-address-bar-in-mobile-browser-to-match-your-wordpress-site/
    √ - Open Graph

# Audits
  √ - WAVE - Chrome plugin - https://chrome.google.com/webstore/detail/wave-evaluation-tool/jbbplnpkjmmeebjpijfedlgcdilocofh?hl=en-US
  √ - Run Chrome UX Audit - lighthouse (go thru each and fix)
  √ - Speed via network panel
  √ - Prod Push
    √ - MailChimp signup and form api
    √ - Deploy to production

# SEO Part 2
√ - submit sitemap in GSC - https://search.google.com/search-console/about
√ - Claim brands
  √ - Twitter
    √ - Brand twitter account
    √ - Avatar for twitter
    √ - Twitter meta tags

    √ - youTube -  https://www.youtube.com/channel/UC5OK0BXUExtLhdAt_lk6NsA
    ~ insta - requires to convert personal account
    ~ linkedin (personal and company)
    √ - facebook
    √ - medium - https://medium.com/@nerdswithcharisma
  √ - Setup Google My business - https://www.google.com/business/
  √ - google and bing site validation
  √ - Schema Data - view-source:https://onedesigncompany.com/


# Extra
  √ Slack notifications with Netlify deployment
  √ Animated SVG menu item
  √ gtm / Hotjar
  a/b testing (google optimize)
- Local SEO - https://www.impulsecreative.com/hubfs/CTAs/Beginner's-Guide-Local-SEO.pdf?hsCtaTracking=1358c6d5-5f85-4c42-bbd0-0bdab1c0aba7%7C481588c9-8b94-4947-b135-56da328e9deb
-  Get data from contentful, backup with gatsby-config.js
- Social
  - Woorank - https://www.woorank.com/en/www/nerdswithcharisma.com
  - todo: break it down for each element (keyword research etc...)
- show how to setup Buy me a coffee
- Getting location data,
- keyboard clicks, esc to close portfolio?
- working with google maps
- Netlify forms
- esLint AirB&B standard
- Design mailchimp template & automate sending to subscribers





# SEO Part 3


# Extra
  Write a roadmap for where and when we cover each tech

# Meta data to do
      <meta name="theme-color" content="#9012fe" />
      <meta name="google-site-verification" content="" />
      <meta name="apple-mobile-web-app-capable" content="yes" />
      <meta name="robots" content="index, follow" />


      <meta property="og:title" content="Nerds With Charisma" />
      <meta property="og:site_name" content="Nerds With Charisma" />
      <meta property="og:url" content="https://nerdswithcharisma.com" />
      <meta property="og:type" content="website" />
      <meta
        property="og:description"
        content="Nerds With Charisma is a cutting edge digital media agency specializing in awesome websites."
      />
      <meta
        property="og:image"
        content="https://nerdswithcharisma.com/images/nwc-tile.png"
      />

      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content="https://nerdswithcharisma.com" />
      <meta name="twitter:creator" content="Brian Dausman" />
      <meta name="twitter:title" content="Nerds With Charisma" />
      <meta
        name="twitter:description"
        content="Nerds With Charisma is a cutting edge digital media agency specializing in awesome websites."
      />
      <meta name="twitter:image:src" content="/images/nwc-tile.png" />

      <meta name="google-site-verification" content="VNLuFr6uYVgjRt5AipwVIwD4ZP6wx6toZPoupfUoyvA" />

infographic stuff - https://moz.com/blog/the-noob-guide-to-online-marketing-with-giant-infographic-11928
Auto post from medium to twitter/fb/insta/etc

# gradient background animation
https://codepen.io/P1N2O/pen/pyBNzX

##  Important links
How to create an npm package - https://medium.freecodecamp.org/how-to-make-a-beautiful-tiny-npm-package-and-publish-it-2881d4307f78

npm login
email:
un: nerds-with-charisma
-->
