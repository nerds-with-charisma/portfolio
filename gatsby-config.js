require('dotenv').config({ path: '.env' });

module.exports = {
  siteMetadata: {
    useContentful: true,
    siteUrl: `https://nerdswithcharisma.com`,
    coffeeUrl: 'https://www.buymeacoffee.com/yz6MAXbTE',
    googleMapsApiKey: 'AIzaSyCuke3pMotG7Ti70NQEwD6PiuCUzoOyIN4',
    homepageData: {
      title:
        'Nerds With Charisma Awesome Websites, Brian Dausman & Andrew Bieganski',
      lang: 'en',
      meta: [
        {
          name: 'description',
          content:
            'Nerds With Charisma are React developers making awesome websites & websites for non-profits, handcrafted by Brian Dausman & Andrew Biegasnki. Web Design Chicago',
        },
        {
          name: 'keywords',
          content:
            'Nerds with Charisma, Brian Dausman, Andrew Bieganski, Web Design Chicago, Awesome Websites, React Developers, Websites for Non-Profits, Bitchin Websites',
        },
        {
          name: 'twitter:card',
          content: 'summary_large_image',
        },
        {
          name: 'twitter:site',
          content: 'https://nerdswithcharisma.com',
        },
        {
          name: 'twitter:creator',
          content: 'Brian Dausman',
        },
        {
          name: 'twitter:title',
          content: 'Nerds With Charisma',
        },
        {
          name: 'twitter:description',
          content:
            'Nerds With Charisma is a cutting edge digital media agency specializing in awesome websites.',
        },
        {
          name: 'twitter:image:src',
          content: 'https://nerdswithcharisma.com/nwc-tile.jpg',
        },
        {
          name: 'p:domain_verify',
          content: '6b1bf384b5f509e1ffd1e8cf6307730c',
        },
      ],
      og: {
        title: 'Nerds With Charisma',
        site_name: 'Nerds With Charisma',
        url: 'https://nerdswithcharisma.com',
        type: 'website',
        description:
          'Nerds With Charisma is a cutting edge digital media agency specializing in awesome websites.',
        image: 'https://nerdswithcharisma.com/nwc-tile.jpg',
      },
    },
    heroData: {
      letter: 'N',
      contactButtonText: 'Get In Touch',
      heroCopy: ['websites.', 'apps.', 'seo.', 'better.'],
      randomQuote: [
        'Coding Since Before We Could Walk.',
        'The Best in the World at What We Do.',
        'Super-Awesome Apps for Super-Awesome Peeps.',
        "I Think We're Gonna Need A Bigger Boat.",
        'Coding Ninjas.',
        'Punk Rock Pixels.',
        'The Best There Is, Was, and Ever Will Be.',
        'Do Or Do Not. There Is No Try.',
        'We Know "The Google".',
        'FOR SCIENCE!',
        'Reinforced by Space-Age Technology.',
      ],
    },
    servicesData: {
      letter: 'W',
      tagline:
        'Here are some of the ways <h2 class="seo--heading">Nerds With Charisma</h2> can help you build an <h2 class="seo--heading">awesome website</h2>',
      services: [
        {
          title: 'DEVELOPMENT',
          items: [
            'Full-Stack - Node / React / GraphQL',
            'JamStack',
            'Mobile App / React Native',
            'Wordpress Websites',
            'SEO & Marketing',
            'Analytics, Tagging, Tracking Pixels',
            'Backups & Monitoring',
            'eCommerce',
          ],
        },
        {
          title: 'DESIGN & SEO',
          items: [
            'Business Cards',
            'Logos & Branding',
            'Stationary',
            'E-Blasts',
            'Social & Strategy',
            'Keyword Research',
            'Analytics',
            'And More...',
          ],
        },
      ],
      logos: [
        {
          url: '/logos/nodejs.svg',
          gridSize: '4',
          title: 'Node JS',
        },
        {
          url: '/logos/react.svg',
          gridSize: '4',
          title: 'ReactJs & React Native',
        },
        {
          url: '/logos/graphql.svg',
          gridSize: '4',
          title: 'GraphQl & Prisma',
        },
        {
          url: '/logos/wordpress.svg',
          gridSize: '3',
          title: 'Wordpress Development',
        },
        {
          url: '/logos/google.svg',
          gridSize: '3',
          title: 'Google Analytics',
        },
        {
          url: '/logos/magento.svg',
          gridSize: '3',
          title: 'Magento',
        },
        {
          url: '/logos/photoshop.svg',
          gridSize: '3',
          title: 'Design / Adobe Creative Suite',
        },
        {
          url: '/logos/gitlab.svg',
          gridSize: '4',
          title: 'Consulting',
        },
        {
          url: '/logos/html5.svg',
          gridSize: '4',
          title: 'HTML5, Web Development',
        },
        {
          url: '/logos/css3.svg',
          gridSize: '4',
          title: 'CSS3, Design and UX',
        },
      ],
    },
    portfolioData: {
      tagline:
        'Checkout some of the <h2 class="seo--heading">awesome websites</h2> and apps as well as <h2 class="seo--heading">websites for non-profits</h2> we\'ve worked on',
      portfolioData: [
        {
          about:
            '<div>This was a fun project. Daily Fit Blend is a startup company that approached us to design and build their website meant to collect sign-ups and provide info about their product.<br /><br />The project was built using the MERN stack (MongoDB, Express, React, and NodeJs) and deployed to Heroku.<br /><br />We also provided SEO support and marketing strategy to set the company up for continued growth.</div>',
          src: '/portfolio/thumb--daily-fit-blend.jpg',
          alt: 'Daily Fit Blend',
          type: 'Design, Marketing & Website',
          images: [
            '/portfolio/daily-fit-blend3.jpg',
            '/portfolio/daily-fit-blend1.jpg',
            '/portfolio/daily-fit-blend2.jpg',
          ],
          id: '25b81e3a-e6cd-58af-bd71-23a5b7832d59',
          website: 'http://www.dailyfitblend.com/',
          hero:
            '//images.ctfassets.net/mdcl40cos6bh/1FegGlBukcyIb4ihVsMS27/65397fff93791ad4848c7ffb9a3343bb/hero--daily-fit-blend.jpg',
          tech:
            'NodeJs, React, MongoDb, HTML5/CSS3, Google Analytics, Marketing, SEO, Design, Web Development',
          launchDate: '2018',
          browser:
            '//images.ctfassets.net/mdcl40cos6bh/hFUtqzTzI1s0subUdILlh/f8d6a71070fcabaa08b9ac97cd273f06/daily-fit-blend-1.jpg',
          sort: 0,
        },
        {
          about:
            "<div>Nerd Fit is a lifestyle app meant to keep your life in order. It tracks your calories burned, steps, workouts, todos, grocery list, and more.<br /><br />This was an amazing project to build, and probably the most fun we've had on a project in a long time.<br /><br />NF will be in the appstore shortly.</div>",
          src: '/portfolio/thumb--nerd-fit.jpg',
          alt: 'Nerd Fit',
          type: 'Design, React Native Application',
          images: [
            '/portfolio/nerd-fit1.jpg',
            '/portfolio/nerd-fit3.jpg',
            '/portfolio/nerd-fit2.jpg',
          ],
          id: 'feb48cc0-0474-5827-8181-8341fb7c6ad2',
          website: null,
          hero:
            '//images.ctfassets.net/mdcl40cos6bh/2yHLzoHjK7xvfVjx40HGNn/213e974741f532f39d1efd73cc1e0092/hero--nerd-fit.jpg',
          tech: 'React Native, GraphQL',
          launchDate: '2019',
          browser:
            '//images.ctfassets.net/mdcl40cos6bh/38Ly2Ula6R16BK4qJ5szS8/e9697441edb790f74ed2e529b77f7241/nerd-fit3.jpg',
          sort: 1,
        },
        {
          about:
            "<div>Sears Hometown & Outlet has been my full-time home for the past 5+ years. In the time there I have worked on so many projects and had the privilege of leading the development lifecycle, using the latest and greatest cutting edge technologies. Nowhere else that I've worked has been so encouraging of using and adopting new technologies.<br /><br />Some of the projects involved at SHO include, the bread and butter website, SearsOutlet, the more rural webistes Sears Hometown, Sears Hardwar...",
          src: '/portfolio/thumb--sears-outlet.jpg',
          alt: 'Sears Hometown & Outlet',
          type: 'Web Development, Marketing, eCommerce',
          images: [
            '/portfolio/shos1.jpg',
            '/portfolio/shos3.jpg',
            '/portfolio/shos2.jpg',
          ],
          id: '88cbd504-21a4-5fff-9ef1-c793faa22562',
          website: 'https://www.searsoutlet.com',
          hero:
            '//images.ctfassets.net/mdcl40cos6bh/66fT1XCi0QPhU5r8YjY0vo/83a48932752d3c1b0bb9088e5acc2e15/hero--outlet.jpg',
          tech:
            'ReactJs, Analytics, Marketing, SEO, Branding, Design, Wordpress, NetSuite / Suite Commerce Advanced, BackboneJs',
          launchDate: '2014',
          browser:
            '//images.ctfassets.net/mdcl40cos6bh/6kmo9hrvGRmfz2vYugZz5w/1c173f069126d9b4e54af7ac9a49c783/browser--outlet.jpg',
          sort: 2,
        },
        {
          about:
            '<div>The great guys at Chamberlain came to us to help get their online presence setup. This included their initial branding and online portal.<br /><br />We worked hand and hand with them to create an experience tailor made for their clients, as well as streamline their existing processes.</div>',
          src: '/portfolio/thumb--chamberlain.jpg',
          alt: 'Chamberlain Staffing',
          type: 'Web Development & Branding',
          images: [
            '/portfolio/chamberlain1.jpg',
            '/portfolio/chamberlain2.jpg',
          ],
          id: '63526ebd-9e23-57e1-baf9-7c795eb20fb9',
          website: 'http://chamberlainstaffing.com',
          hero:
            '//images.ctfassets.net/mdcl40cos6bh/6rFewgGNW2IMN3BeM8D76h/bc1ce2ba2de0602300bbf5e250fa5607/hero--chamberlain.jpg',
          tech: 'Angular, Design, Branding, Marketing, SEO',
          launchDate: '2016',
          browser: null,
          sort: 3,
        },
        {
          about:
            '<div>For Axiom we did a complete and total re-brand. Everything from the ground up was reworked, redesigned, and built from scratch.<br /><br/ >Marketing collateral, business cards, internal documents, resumes, website, internal applications, etc...<br /><br />This was a long project over the course of several years. Everything from the lowly pixel, to giant SEO related tasks were fair game.<br /><br />Axiom was tons of fun to work on, in a true end-to-end fashion.</div>  ',
          src: '/portfolio/thumb--axiom.jpg',
          alt: 'Axiom Technology Group',
          type: 'Contract',
          images: ['/portfolio/axiom1.jpg'],
          id: '6dab625f-f973-5867-9931-b791469f7d85',
          website: 'http://axiomtechgroup.com',
          hero:
            '//images.ctfassets.net/mdcl40cos6bh/7CKhQHDA21ZBjBR7npjt0E/76e6a19f7c625a871fdb13bc570df2ee/hero--axiom.jpg',
          tech:
            'Wordpress, PHP, HTML5/CSS3, Design, Custom App. Dev., Marketing, SEO, Branding, Everything In-Between',
          launchDate: '2016',
          browser:
            '//images.ctfassets.net/mdcl40cos6bh/76UkWpBHD62c2W82aePjYn/9316d635bf2dcd5da0a51d950bc563e2/browser--axiom.jpg',
          sort: 4,
        },
        {
          about:
            '<div>Camp Play-A-Lot is one of our favorite Not-For-Profits<br /><br />We helped setup their website and hosting, setup and install a premium Wordpress theme, and taught them how to maintain everything on their own!</div> ',
          src: '/portfolio/thumb--camp-play-alot.jpg',
          alt: 'Camp Play-A-Lot',
          type: 'Wordpress',
          images: null,
          id: 'c54ac7e2-8b0a-56df-8520-1698de5be966',
          website: 'http://campplayalot.us/',
          hero:
            '//images.ctfassets.net/mdcl40cos6bh/FyKVv3ebvaSj1xlgdUSXz/a909441fd5f168719c6aafbfda3ea3ea/hero--camp-play-a-lot.jpg',
          tech: 'Wordpress, Themeing, Widget Dev',
          launchDate: '2017',
          browser:
            '//images.ctfassets.net/mdcl40cos6bh/2RQ94PzMhwYddBWdnYkgFL/4046480507f87b62150e8025262db534/browser-camp-play-a-lot.jpg',
          sort: 5,
        },
        {
          about:
            '<div>TPSR Alliance is another non-for-profit we really enjoyed working with.<br /><br />We helped them setup their web presence and created a custom Wordpress site to keep visitors up-to-date on all their latest info as well as track attendance at their conferences.<br /><br />Along with setting everything up, we taught them how to maintain their website to so it could continue to grow!</div>',
          src: '/portfolio/thumb--tpsr.jpg',
          alt: 'TPSR Alliance',
          type: 'Web Development, Wordpress',
          images: null,
          id: '608ae428-d498-5914-af46-25913cf50023',
          website: 'https://www.tpsr-alliance.org/',
          hero:
            '//images.ctfassets.net/mdcl40cos6bh/yc1DbRHoET5u4y27idWsc/e640465598815e8ad500f7024d0a1737/hero--tpsr.jpg',
          tech: 'Wordpress, Responsive',
          launchDate: '2013',
          browser:
            '//images.ctfassets.net/mdcl40cos6bh/3ntAboYUib1TWEfmGRjd91/1712ae4a37928255858e35dfaa452a23/browser--tpsr.jpg',
          sort: 6,
        },
        {
          about:
            '<div>Premier Academy was one of those success stories we really enjoy telling. We built their website, and handed it off to them to maintain internally. Not only did they excell at this, but after years they were able to grow it on their own and customize it beyond our wildest dreams. The teachers at PA were so eager to learn, and so excited to get their hands on the finished project, they took it over and ran with it. What lives today is more a testiment to their work their ours!</div>',
          src: '/portfolio/thumb--premier-academy.jpg',
          alt: 'Premier Academy',
          type: 'Web Development',
          images: ['/portfolio/premier1.jpg', '/portfolio/premier2.jpg'],
          id: '115e3216-7a61-5430-9d8e-93190c91883a',
          website: 'https://premieracademymi.com/',
          hero:
            '//images.ctfassets.net/mdcl40cos6bh/6T55NsA9P8tVpS6YAMKFXZ/5ec12a072596234886918c1e49cf5cf7/hero--premier-academy.jpg',
          tech: 'Wordpress, Design, SEO',
          launchDate: '2015',
          browser: null,
          sort: 7,
        },
        {
          about:
            "<div>Ladse is one of our favorite clients. Just like others we've worked with, they wanted something they could 'own'. Unhappy with their currently hosted solution that was updated by a 3rd party, Ladse wanted a website they could manage and update on their own. For several years they have been managing things like pros! We love that the on-going relationship with them is more guidance and learning than hands-on coding. They have made the website a full online resource and community that th...",
          src: '/portfolio/thumb--ladse.jpg',
          alt: 'LADSE',
          type: 'Web Development',
          images: ['/portfolio/ladse1.jpg'],
          id: '7a039681-e838-55cf-820c-01cdfce8a2f0',
          website: 'http://ladse.org',
          hero:
            '//images.ctfassets.net/mdcl40cos6bh/1dESV9Cb6Kgx55ovf2BJdq/b5fa6a0db09a4d27a2a85196bd237839/hero--ladse.jpg',
          tech: 'Wordpress, Design, SEO',
          launchDate: '2016',
          browser: null,
          sort: 8,
        },
        {
          about:
            '<div>We did a lot of work for the Oak Brook Chamber. Many late nights, and intense discussions ensued. Shortly after, several websites were born to help people find what to do in OB, including dinner, drinks, events, Chamber meetings, and more.<br/ ><br />The goal of this project was to create a one stop shop to everything OB. Working with several of the local businesses, board members, and community volunteers were the true highlight of this project.</div>',
          src: '/portfolio/thumb--ob-chamber.jpg',
          alt: 'Oak Brook Chamber of Commerce',
          type: 'Marketing & Web Development',
          images: null,
          id: '1410705a-e907-599d-b43d-f676f5b44590',
          website: 'https://www.obchamber.com/',
          hero:
            '//images.ctfassets.net/mdcl40cos6bh/4OPBI1WVmGYkF6kh7hDnlt/3c1727b5373d978bf05f54802b2dc938/hero--oakbrook.jpg',
          tech: 'Wordpress, Design, SEO, Marketing',
          launchDate: '2014',
          browser: null,
          sort: 9,
        },
        {
          about:
            "<div>We absolutely LOVE working with charities, infact, looking back at our portfolio, we've worked with a lot!<br /><br />SOS Illinois, we had the privilage to consult with for several years, maintaining and updating their website. This project involved building them a new online presence and offering marketing advice. It's been several years since we worked on this project, and their ownership has changed hands, but we still very much enjoyed working with them.</div>  ",
          src: '/portfolio/thumb--sos-illinois.jpg',
          alt: 'SOS Illinois',
          type: 'Web Development',
          images: ['/portfolio/sos1.jpg'],
          id: '51a9e056-6528-566f-a78d-b2daba1811ba',
          website: null,
          hero:
            '//images.ctfassets.net/mdcl40cos6bh/5r9TLXDvXfM12WPA0dWQBl/40e951c163f55fa57217aa519b392a1a/hero--sos.jpg',
          tech: 'Wordpress, Design, Marketing, SEO',
          launchDate: '2014',
          browser: null,
          sort: 10,
        },
        {
          about:
            "<div>My Stericycle was an internal auditing and reporting tool for the Stericycle company. This app allowed users to take notes, snap pics, tag locations, monitor travel, and more. This project has some very unique challenges that you don't see very often.</div>",
          src: '/portfolio/thumb--stericycle.jpg',
          alt: 'My Stericycle',
          type: 'Application Development',
          images: null,
          id: 'f7ab1544-2359-55ae-aafb-7da21574d919',
          website: null,
          hero:
            '//images.ctfassets.net/mdcl40cos6bh/5OSNxpUz47XifxZkTV3y6b/7f4f3a3222ea45451ea4f4b136598753/hero--mystericycle.jpg',
          tech: 'Design, HTML5/CSS3, Phone Gap',
          launchDate: '2014',
          browser: null,
          sort: 11,
        },
        {
          about:
            "<div>BFG is where we got our start. The first place to take a chance on us so long ago. This was truly a great group of people.<br /><br />Hardcore gamers, passionate workers, skilled craftsman. BFG pushed the envelope, pushed boundaries, and broke barriers.<br /><br />Package design, office collateral, banners, branding, this was truly a all-encompassing project. Print, digital, SEO, the works! We also worked with several of BFG's side projects including their Phobos line of enthusiast com...",
          src: '/portfolio/thumb--bfg.jpg',
          alt: 'BFG Tech',
          type: 'Design & Web Development',
          images: [
            '/portfolio/bfg1.jpg',
            '/portfolio/bfg2.jpg',
            '/portfolio/bfg3.jpg',
          ],
          id: 'f0dad8fd-16c2-53b8-8c7a-7d864e1193f6',
          website: null,
          hero:
            '//images.ctfassets.net/mdcl40cos6bh/3RSxqip4ZRelTd58y2QjWV/0453f6a43e4065b51e6c7cd4ee69629a/hero--bfg.jpg',
          tech: 'Little Bit of Everything',
          launchDate: '2008',
          browser: null,
          sort: 12,
        },
        {
          about:
            '<div>We worked with NIU outreach oh so long ago building e-learning modules for teachers to accompany their in-class teaching. These modules allowed interactive learning and navigation through the teachers lectures, allowing the viewer to see accompanying texts, slides, artwork, quotes, audio excerpts, etc.<br/ ><br />Creating e-learning modules were a great learning experience in themselves, as we picked up so many skills, not just from a development stand-point, but also usability. Seeing ...',
          src: '/portfolio/thumb--niu-outreach.jpg',
          alt: 'NIU Outreach',
          type: 'Flash & Design',
          images: null,
          id: 'cd763272-8583-5ff9-aaf5-9adf12e617dc',
          website: null,
          hero:
            '//images.ctfassets.net/mdcl40cos6bh/5pE2z1fJCvYZODGPYZWVwC/4d96bf6cecd9c108bd343b5887023664/hero--niu.jpg',
          tech: 'Flash, AS3, Design',
          launchDate: '2008',
          browser: null,
          sort: 13,
        },
      ],
    },
    aboutData: {
      letter: 'C',
      who: {
        image: '/portfolio/brian.jpg',
        alt:
          "That's me, Brian Dausman, founder of Nerds with Charisma and React Developer",
        body:
          "<div>Nerds With Charisma are a small band of designers, developers, and creatives. Our unique process involves a hands on, back-and-forth approach to get your project off the ground and into the wild exactly as you envision it.<br /><br />Find out how we can bring fresh ideas and a new approach to your company. We don't just build your website, we build your brand and cultivate your online identity. We collaborate closely with you to learn your business, discover new opportunities, and bring your ideas to life.</div>",
      },
      what: {
        image: '/portfolio/andy.jpg',
        alt:
          "That's Andrew Bieganski, he's been with with Nerds with Charisma for years helping build some truely awesome websites!",
        body:
          "<div>We utilize the latest technologies & trends to get your site setup and rocking. Our help doesn't stop at just a website. Sure, we can help design & develop your site, but we will also discuss and explain your user's experience, come up with a content strategy for continued success, get you going on social media, explain the new technologies, go over analytics, and get your SEO juice flowing.<br /><br /></div>",
      },
      where: {
        image: '/portfolio/maeby.jpg',
        alt:
          "That's our project manager and QA expert, Maeby, she's in charge of making sure we continue to make bitchin websites. She prefers when we do websites for non-profits",
        body:
          "We provide development and <h2 class=\"seo--heading\">web design to Chicago</h2> and the surrounding suburbs. We do lots of local work, but are totally willing to do long distance remote work, so long as you're ok with it. We're also very comfortable working with people pretty much anywhere in the world. We're previously worked with people in England, India, South America, and Israel.<br /><br />We also really enjoy doing websites for nonprofits. We can work with you and your budget to get your projects rolling. Simply shoot us an email below, and we'll let you know what we can do.",
      },
    },
    socialLinks: [
      {
        link: 'https://jamstack-jedi.nerdswithcharisma.com/',
        icon: '/logos/logo--jedi.svg',
        title: 'Jamstack Jedi',
      },
      {
        link: 'https://gitlab.com/nerds-with-charisma',
        icon: '/logos/logo--gitlab.svg',
        title: 'GitLab',
      },
      {
        link: 'https://twitter.com/nerdswcharisma',
        icon: '/logos/logo--twitter.svg',
        title: 'Twitter',
      },
      {
        link: 'https://www.npmjs.com/~nerds-with-charisma',
        icon: '/logos/logo--npm.svg',
        title: 'NPM',
      },
      {
        link: 'https://medium.com/@nerdswithcharisma',
        icon: '/logos/logo--medium.svg',
        title: 'Medium',
      },
    ],
    title: `Nerds With Charisma`,
    signOff: `
      <strong>
       Designed &amp; Developed By
      <br />
      <a href="/docs/brian-dausman-resume-2019.pdf">
        <h2 class="font--16 font--primary inline">
          brian dausman
        </h2>
      </a>
       +
      <a href="http://www.andrewbieganski.com/images/andrewbieganski2018.pdf" target="_blank" rel="noopener noreferrer">
        <h2 class="font--16 font--primary inline">
          andrew bieganski
        </h2>
      </a>
    </strong>`,
    footerTagline:
      'NWC making <h2 class="seo--heading">bitchin websites</h2> since the 90s!',
    navigation: ['home', 'services', 'portfolio', 'about', 'contact'],
    description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
    author: `@gatsbyjs`,
  },
  plugins: [
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: process.env.CONTENTFUL_SPACE_ID,
        accessToken: process.env.CONTENTFUL_DELIVERY_ACCESS_TOKEN,
      },
    },
    'gatsby-plugin-eslint',
    {
      resolve: 'gatsby-plugin-mailchimp',
      options: {
        endpoint:
          'https://gmail.us20.list-manage.com/subscribe/post?u=8914e75fae490805be15a9e74&amp;id=3ca0742e30',
      },
    },
    `gatsby-plugin-sitemap`,
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        host: 'https://nerdswithcharisma.com',
        sitemap: 'https://nerdswithcharisma.com/sitemap.xml',
        policy: [{ userAgent: '*', allow: '/' }],
      },
    },
    {
      resolve: `gatsby-plugin-canonical-urls`,
      options: {
        siteUrl: `https://nerdswithcharisma.com`,
      },
    },
    `gatsby-plugin-sass`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/logo@2x.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: 'gatsby-plugin-google-tagmanager',
      options: {
        id: 'GTM-NP7BCNC',
        includeInDevelopment: true,
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
};
