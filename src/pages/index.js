import React, { useState, useEffect } from 'react';
import { PropTypes } from 'prop-types';
import { StaticQuery, graphql } from 'gatsby';

import Layout from '../components/layout';

import Header from '../components/header/Header';
import Hero from '../components/hero/Hero';
import SEO from '../components/seo';
import Services from '../components/services/Services';
import Portfolio from '../components/portfolio/Portfolio';
import About from '../components/about/About';
import Contact from '../components/contact/Contact';
import Footer from '../components/footer/Footer';

const IndexPage = ({ pageContext }) => (
  <StaticQuery
    query={graphql`
      query IndexQuery {
        allContentfulPortfolio(sort: { fields: sort }) {
          edges {
            node {
              alt
              src
              type
              website
              about {
                content {
                  content {
                    value
                  }
                }
              }
              images
              hero
              tech
              launchDate
              sort
              browser
            }
          }
        }

        site {
          siteMetadata {
            useContentful
            coffeeUrl
            googleMapsApiKey
            title
            signOff
            footerTagline
            navigation
            homepageData {
              title
              lang
              meta {
                name
                content
              }
              og {
                title
                site_name
                url
                type
                description
                image
              }
            }
            servicesData {
              letter
              tagline
              services {
                title
                items
              }
              logos {
                url
                gridSize
                title
              }
            }
            portfolioData {
              tagline
              portfolioData {
                alt
                src
                type
                website
                about
                images
                hero
                tech
                launchDate
                sort
                browser
              }
            }
            aboutData {
              letter
              who {
                image
                alt
                body
              }
              what {
                image
                alt
                body
              }
              where {
                image
                alt
                body
              }
            }
          }
        }
      }
    `}
    render={(data) => {
      const [supportsWebP, setSupportsWebP] = useState(true);
      const [currentProjectToOpen, setCurrentProjectToOpen] = useState(null);

      // will run once, when the component mounts
      useEffect(() => {
        // check if we can use webP images
        setSupportsWebP(
          /Chrome/.test(window.navigator.userAgent) &&
            /Google Inc/.test(window.navigator.vendor),
        );

        // after the page load, lets check if we should load a portfolio item right away
        if (pageContext.currentProjectToOpen) {
          setCurrentProjectToOpen(pageContext.currentProjectToOpen);
        }
      }, []);

      return (
        <Layout>
          <SEO
            googleMapsApiKey={data.site.siteMetadata.googleMapsApiKey}
            title={data.site.siteMetadata.homepageData.title}
            lang={data.site.siteMetadata.homepageData.lang}
            meta={data.site.siteMetadata.homepageData.meta}
            og={data.site.siteMetadata.homepageData.og}
          />
          <Header
            title={data.site.siteMetadata.title}
            navigation={data.site.siteMetadata.navigation}
          />
          <Hero supportsWebP={supportsWebP} />
          <Services servicesData={data.site.siteMetadata.servicesData} />
          <Portfolio
            currentProjectToOpen={currentProjectToOpen}
            portfolioData={data.site.siteMetadata.portfolioData}
            dynamicPortfolioData={data.allContentfulPortfolio.edges}
            useContentful={data.site.siteMetadata.useContentful}
          />
          <About aboutData={data.site.siteMetadata.aboutData} city={null} />
          <Contact />
          <Footer
            title={data.site.siteMetadata.title}
            signOff={data.site.siteMetadata.signOff}
            footerTagline={data.site.siteMetadata.footerTagline}
            coffeeUrl={data.site.siteMetadata.coffeeUrl}
          />
        </Layout>
      );
    }}
  />
);

IndexPage.propTypes = {
  pageContext: PropTypes.object.isRequired,
};

export default IndexPage;
