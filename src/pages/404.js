import React from 'react';
import Helmet from 'react-helmet';

import '../styles/404.scss';

export default () => (
  <div id="fourOhFour">
    <Helmet title="Page Not Found" htmlAttributes={{ lang: 'en' }} />
    <div>
      <aside>
        <img
          width="50"
          src="/404/logo--nwc-purple.svg"
          alt="Nerds With Charisma Logo"
        />
        <h1>{'404'}</h1>
        <img className="hero" src="/404/404.gif" alt="Bummer..." />
        <br />
        <br />
        <strong>{"We couldn't find what you're looking for."}</strong>
        <br />
        <br />
        <a href="/">{'GO HOME'}</a>
      </aside>
    </div>
  </div>
);
