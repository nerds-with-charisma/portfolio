import React from 'react';
import { PropTypes } from 'prop-types';

const PortfolioFooter = ({ website, launchDate }) => (
  <section id="portfolioItem--footer" className="text-center">
    {launchDate && (
      <strong className="font--80">{`Launched In ${launchDate}`}</strong>
    )}
    <br />
    <br />
    {website && (
      <a
        href={website}
        target="_blank"
        rel="no-follow noopener noreferrer"
        className="btn btn--gradient radius--lg font--28"
      >
        <strong className="font--light font--21">{'Visit Website'}</strong>
      </a>
    )}
  </section>
);

PortfolioFooter.defaultProps = {
  website: null,
  launchDate: null,
};

PortfolioFooter.propTypes = {
  website: PropTypes.string,
  launchDate: PropTypes.string,
};

export default PortfolioFooter;
