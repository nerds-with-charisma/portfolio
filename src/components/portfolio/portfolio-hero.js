import React from 'react';
import { PropTypes } from 'prop-types';

const PortfolioHero = ({ alt, hero }) => (
  <section id="portfolioItem--hero">
    <img src={hero} alt={`${alt} hero banner`} />
    <h2>{alt}</h2>
  </section>
);

PortfolioHero.propTypes = {
  alt: PropTypes.string.isRequired,
  hero: PropTypes.string.isRequired,
};

export default PortfolioHero;
