import React from 'react';
import { PropTypes } from 'prop-types';

const PortfolioItem = ({ item, setItem }) => (
  <figure className="brick">
    <button type="button" onClick={() => setItem(item)}>
      <img alt={item.alt} src={item.src} />
    </button>
  </figure>
);

PortfolioItem.propTypes = {
  item: PropTypes.object.isRequired,
  setItem: PropTypes.func.isRequired,
};

export default PortfolioItem;
