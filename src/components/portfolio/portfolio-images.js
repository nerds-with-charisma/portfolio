import React from 'react';
import { PropTypes } from 'prop-types';

const PortfolioImages = ({ images }) => (
  <section id="portfolioItem--images">
    {images &&
      images.map((image, i) => (
        <img src={image} alt={`Portfolio shot ${i}`} key={image} />
      ))}
  </section>
);

PortfolioImages.propTypes = {
  images: PropTypes.array.isRequired,
};

export default PortfolioImages;
