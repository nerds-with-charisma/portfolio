import React from 'react';
import { PropTypes } from 'prop-types';

const PortfolioWhat = ({ about, tech }) => (
  <section id="portfolioItem--what">
    <strong className="font--42">{'What we did'}</strong>
    <br />
    <br />
    <div
      className="font--16"
      dangerouslySetInnerHTML={{ __html: about || '' }}
    />
    <br />
    <br />
    <strong>{`[ ${tech} ]`}</strong>
  </section>
);

PortfolioWhat.propTypes = {
  about: PropTypes.string.isRequired,
  tech: PropTypes.string.isRequired,
};

export default PortfolioWhat;
