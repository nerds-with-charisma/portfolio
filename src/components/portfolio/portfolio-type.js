import React from 'react';
import { PropTypes } from 'prop-types';

const PortfolioType = ({ type, browser }) => (
  <section id="portfolioItem--type" className="grid">
    <div className="col-12 col-md-6 col-lg-6">
      <strong className="font--80 lh-sm">{type}</strong>
    </div>
    <div className="col-12 col-md-6 col-lg-6">
      {browser && <img alt={JSON.toString(type)} src={browser} />}
    </div>
  </section>
);

PortfolioType.propTypes = {
  type: PropTypes.string.isRequired,
  browser: PropTypes.string.isRequired,
};

export default PortfolioType;
