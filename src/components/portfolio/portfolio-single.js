import React from 'react';
import { PropTypes } from 'prop-types';

import PortfolioHero from './portfolio-hero';
import PortfolioType from './portfolio-type';
import PortfolioWhat from './portfolio-what';
import PortfolioImages from './portfolio-images';
import PortfolioFooter from './portfolio-footer';

const PortfolioSingle = ({ item, setItem }) => (
  <aside id="portfolioSingle" className={item !== null ? 'active' : 'inactive'}>
    {item !== null && (
      <>
        <button type="button" className="close" onClick={() => setItem(null)}>
          &times;
        </button>

        <PortfolioHero alt={item.alt} hero={item.hero} />
        <br />
        <br />
        <section className="container-md">
          <PortfolioType type={item.type} browser={item.browser} />
          <PortfolioWhat
            about={
              item.about.content
                ? item.about.content[0].content[0].value
                : item.about
            }
            tech={item.tech}
          />
          <PortfolioImages images={item.images} />
          <PortfolioFooter
            website={item.website}
            launchDate={item.launchDate}
          />
        </section>
      </>
    )}
  </aside>
);

PortfolioSingle.defaultProps = {
  item: null,
};

PortfolioSingle.propTypes = {
  item: PropTypes.object,
  setItem: PropTypes.func.isRequired,
};

export default PortfolioSingle;
