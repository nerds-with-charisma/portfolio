import React, { useState } from 'react';
import addToMailchimp from 'gatsby-plugin-mailchimp';

const Contact = () => {
  const [email, setEmail] = useState(null);
  const [message, setMessage] = useState(null);
  const [messageError, setMessageError] = useState(null);
  const [messageSuccess, setMessageSuccess] = useState(null);

  const handleSubmit = (e) => {
    e.preventDefault();

    setMessageError(null); // remove the message each time they submit

    const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // eslint-disable-line
    if (!regex.test(email)) {
      setMessageError('Please enter a valid e-mail.');
      return false;
    }

    if (!message || message.length < 2) {
      setMessageError('Please enter a valid message.');
      return false;
    }

    addToMailchimp(email, {
      MESSAGE: message,
    }).then((data) => {
      if (data.result === 'error') {
        setMessageError(data.msg);
      } else {
        setMessageSuccess("Thanks, we'll get back to you shortly!");
      }
    });

    return true;
  };

  return (
    <section id="contact" className="text-center bg--offLight padding-lg">
      <strong className="font--48">{'What can we help you with?'}</strong>
      <br />
      <br />
      {messageError && (
        <span
          className="bg--cta font--light padding-sm"
          dangerouslySetInnerHTML={{ __html: messageError }}
        />
      )}

      {messageSuccess ? (
        <span className="bg--success font--light padding-sm">
          {messageSuccess}
        </span>
      ) : (
        <form id="contactForm" onSubmit={(e) => handleSubmit(e)}>
          <label htmlFor="email">
            <span>Email</span>
            <input
              type="email"
              id="email"
              name="email"
              placeholder="Your Email"
              onChange={(e) => setEmail(e.target.value)}
            />
          </label>
          <label htmlFor="message">
            <span>Message</span>
            <input
              type="message"
              id="message"
              name="message"
              placeholder="Your Message"
              onChange={(e) => setMessage(e.target.value)}
            />
          </label>

          <button type="submit" className="btn btn--gradient">
            {'Contact Us!'}
          </button>
        </form>
      )}
    </section>
  );
};

export default Contact;
