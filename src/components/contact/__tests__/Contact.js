import React from 'react';
import { render } from '@testing-library/react';
import Contact from '../Contact';

test('subscribe renders email and message', () => {
  // get the element and extrapolate the label text and debug func
  const { getByLabelText, debug } = render(<Contact />);

  debug(); // will print out the el you got above

  // find the email and message input
  expect(getByLabelText(/email/i).id).toEqual('email');
  expect(getByLabelText(/message/i).id); // more than one way of doing it
});
