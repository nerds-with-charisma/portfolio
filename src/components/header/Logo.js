import React from 'react';
import { PropTypes } from 'prop-types';

import LogoSvg from '../../images/logo.svg';

const Logo = ({ title }) => (
  <a href="/" id="logo" className="font--light transition--all">
    <img src={LogoSvg} alt={`${title} Logo`} />
    <h1 className="ir">{title}</h1>
  </a>
);

Logo.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Logo;
