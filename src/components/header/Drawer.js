import React from 'react';
import { PropTypes } from 'prop-types';

import Logo from '../../images/logo.svg';

const Drawer = ({ isOpen, navigation, scrollToSection }) => (
  <nav id="nav" className={isOpen ? 'in' : 'out'}>
    <br />
    <img src={Logo} alt="Menu Logo" />
    <br />
    <br />
    {navigation.map((link) => (
      <React.Fragment key={link}>
        <button
          type="button"
          className="nav--item font--light border--none font--18"
          onClick={() => scrollToSection(link)}
        >
          <strong>{link.toUpperCase()}</strong>
        </button>
        <br />
      </React.Fragment>
    ))}

    <br />
    <br />

    <a
      href="https://jamstack-jedi.nerdswithcharisma.com/"
      className="nav--item font--light border--none font--18 padding-md"
    >
      <strong>{'JAMSTACK JEDI'}</strong>
    </a>
    <br />
  </nav>
);

Drawer.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  navigation: PropTypes.array.isRequired,
  scrollToSection: PropTypes.func.isRequired,
};

export default Drawer;
