import React, { useState, useEffect } from 'react';
import { PropTypes } from 'prop-types';

import Drawer from './Drawer';

const Nav = ({ navigation }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [isDark, setIsDark] = useState(false);

  const handleScroll = () => {
    const services = document.getElementById('services');
    const about = document.getElementById('about');

    const inLightDiv =
      (services.getBoundingClientRect().top <= 0 &&
        -1 * services.getBoundingClientRect().top <= services.offsetHeight) ||
      (about.getBoundingClientRect().top <= 0 &&
        -1 * about.getBoundingClientRect().top <= about.offsetHeight);

    if (inLightDiv) {
      setIsDark(true);
    } else {
      setIsDark(false);
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
  }, []);

  const scrollToSection = (link) => {
    setIsOpen(false);

    window.dataLayer.push({
      event: 'navClick',
      navClickLabel: link,
    });

    document.getElementById(link).scrollIntoView({ behavior: 'smooth' });
  };

  return (
    <section id="headerNav">
      <button
        className={isDark ? 'isDark' : 'font--light'}
        type="button"
        onClick={() => setIsOpen(!isOpen)}
        id="nav--btn"
      >
        <div className={isOpen ? 'nav--svg open' : 'nav--svg'}>
          <span />
          <strong className="ir">Menu</strong>
        </div>
      </button>
      <Drawer
        isOpen={isOpen}
        navigation={navigation}
        scrollToSection={scrollToSection}
      />
    </section>
  );
};

Nav.propTypes = {
  navigation: PropTypes.array.isRequired,
};

export default Nav;
