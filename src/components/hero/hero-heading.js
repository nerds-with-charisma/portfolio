import React from 'react';
import { PropTypes } from 'prop-types';

const HeroHeading = ({ heroCopy }) => (
  <h2 className="font--125">
    {heroCopy.map((copy) => (
      <React.Fragment key={copy}>
        {copy}
        <br />
      </React.Fragment>
    ))}
  </h2>
);

HeroHeading.propTypes = {
  heroCopy: PropTypes.array.isRequired,
};

export default HeroHeading;
