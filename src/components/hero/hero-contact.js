import React from 'react';
import { PropTypes } from 'prop-types';

const HeroContact = ({ contactButtonText }) => (
  <button
    type="button"
    className="btn bg--light font--dark font--24 padding-horiz-xl radius--lg border--none block no--wrap"
    onClick={() =>
      document.getElementById('contact').scrollIntoView({ behavior: 'smooth' })
    }
  >
    <strong>{contactButtonText}</strong>
  </button>
);

HeroContact.propTypes = {
  contactButtonText: PropTypes.string.isRequired,
};

export default HeroContact;
