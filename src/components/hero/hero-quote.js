import React from 'react';
import { PropTypes } from 'prop-types';

const HeroQuote = ({ randomQuote }) => (
  <aside className="font--40">{randomQuote}</aside>
);

HeroQuote.propTypes = {
  randomQuote: PropTypes.string.isRequired,
};

export default HeroQuote;
