import React from 'react';
import { PropTypes } from 'prop-types';
import LazyLoad from 'react-lazyload';

import SocialLinks from '../common/social-links';

const Footer = ({ title, signOff, footerTagline, coffeeUrl }) => (
  <section id="footer" className="container text-center lh-md font--16">
    <br />
    <br />
    <strong>
      <div dangerouslySetInnerHTML={{ __html: signOff || '' }} />
    </strong>

    <br />
    <br />

    <SocialLinks />

    <button
      type="button"
      style={{
        color: '#666',
        border: 'none',
        position: 'fixed',
        bottom: 5,
        right: -15,
        fontSize: 17,
      }}
      onClick={() =>
        console.info(`
      __.-._
      '-._"7'
      /'.-c
      |  /T
     _)_/LI

     Do or do not. There is no try.

      We honestly can't thank you enough for checking out our site.
      The fact you're even digging in here is pretty neat.

      Are you a non-for profit? We love working with them and are totally willing to work with your budget. Just drop us a line.

      Be excellent to each other
        -Bill S. Preston, Esq.
      `)
      } // eslint-disable-line
    >
      &#960;
    </button>

    <br />
    <br />

    <small className="font--text">
      {`copyright © ${new Date().getFullYear()} ${title.toLowerCase()}`}
      <br />
      <span className="font--text opacity8">
        <div dangerouslySetInnerHTML={{ __html: footerTagline }} />
      </span>
    </small>

    <br />

    {coffeeUrl && (
      <div className="hidden-xs hidden-sm">
        <a
          className="bmc-button"
          target="_blank"
          rel="noopener noreferrer"
          href={coffeeUrl}
        >
          <LazyLoad offset={200}>
            <img
              src="https://bmc-cdn.nyc3.digitaloceanspaces.com/BMC-button-images/BMC-btn-logo.svg"
              alt="Buy me a coffee"
            />
          </LazyLoad>
          <span className="font--14">{' Buy me a coffee'}</span>
        </a>
      </div>
    )}

    <br />
  </section>
);

Footer.defaultProps = {
  coffeeUrl: null,
};

Footer.propTypes = {
  coffeeUrl: PropTypes.string,
  title: PropTypes.string.isRequired,
  signOff: PropTypes.string.isRequired,
  footerTagline: PropTypes.string.isRequired,
};

export default Footer;
