import React from 'react';
import { StaticQuery, graphql } from 'gatsby';

const SocialLinks = () => (
  <StaticQuery
    query={graphql`
      query SocialQuery {
        site {
          siteMetadata {
            socialLinks {
              link
              icon
              title
            }
          }
        }
      }
    `}
    render={(data) => (
      <section className="social-links">
        {data.site.siteMetadata.socialLinks.map((link) => (
          <a
            key={link.title}
            className="font--grey padding-md"
            href={link.link}
            target="_blank"
            rel="noopener noreferrer"
          >
            <img src={link.icon} alt={link.title} />
          </a>
        ))}
      </section>
    )}
  />
);

export default SocialLinks;
