import React from 'react';
import { PropTypes } from 'prop-types';
import LazyLoad from 'react-lazyload';

import LargeLetter from '../common/large-letter';

const About = ({ aboutData, city }) => (
  <section
    id="about"
    className="container position--relative overflow--container"
  >
    <span className="opacity1">
      <LargeLetter letter={aboutData.letter} />
    </span>
    <div className="container position--relative">
      <div className="grid">
        <div className="col-12 col-lg-4 text-center">
          <br />
          <br />
          <div>
            <LazyLoad offset={200}>
              <img
                alt={aboutData.who.alt}
                className="radius--lg"
                src={aboutData.who.image}
              />
            </LazyLoad>
          </div>
        </div>
        <div className="col-12 col-lg-8 text-center lh-md">
          <strong>{'WHO'}</strong>
          <hr className="shorty" />
          <br />
          <div dangerouslySetInnerHTML={{ __html: aboutData.who.body }} />
        </div>

        <div className="col-12 col-lg-8 text-center lh-md">
          <br />
          <br />
          <br />
          <strong>{'WHAT'}</strong>
          <hr className="shorty" />
          <br />
          <div dangerouslySetInnerHTML={{ __html: aboutData.what.body }} />
        </div>
        <div className="col-12 col-lg-4 text-center lh-md">
          <br />
          <br />
          <br />
          <div>
            <LazyLoad offset={200}>
              <img
                alt={aboutData.what.alt}
                className="radius--lg"
                src={aboutData.what.image}
              />
            </LazyLoad>
          </div>
        </div>

        <div className="col-12 col-lg-4 text-center">
          <br />
          <br />
          <div>
            <LazyLoad offset={200}>
              <img
                alt={aboutData.where.alt}
                className="radius--lg hidden-xs hidden-sm"
                src={aboutData.where.image}
              />
            </LazyLoad>
          </div>
        </div>
        <div className="col-12 col-lg-8 text-center lh-md">
          <strong>{'WHERE'}</strong>
          <hr className="shorty" />
          <br />
          <div
            dangerouslySetInnerHTML={{
              __html: `
              ${aboutData.where.body}
              <br /><br />
              ${
                city
                  ? `We can totally help you dominate the ${city} SEO market!`
                  : ''
              }
            `,
            }}
          />
        </div>
      </div>
    </div>
  </section>
);

About.defaultProps = {
  city: null,
};

About.propTypes = {
  aboutData: PropTypes.object.isRequired,
  city: PropTypes.string,
};

export default About;
