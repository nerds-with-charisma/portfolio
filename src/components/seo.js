import React from 'react';
import { PropTypes } from 'prop-types';
import Helmet from 'react-helmet';

import schema from '../config/schema-markup';

function SEO({ googleMapsApiKey, title, lang, meta, og }) {
  return (
    <Helmet htmlAttributes={{ lang }} title={title}>
      <script
        async
        defer
        src={`https://maps.googleapis.com/maps/api/js?key=${googleMapsApiKey}`}
      />

      {meta.map((item) => (
        <meta key={item.name} name={item.name} content={item.content} />
      ))}

      {schema.data.map((item) => (
        <script type="application/ld+json" key={item}>
          {JSON.stringify(item).replace(/_/g, '@')}
        </script>
      ))}

      {Object.keys(og).map((key) => (
        <meta property={`og:${key}`} content={og[key]} key={og[key]} />
      ))}

      <link
        rel="alternate stylesheet"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900"
        onLoad="this.rel='stylesheet'"
      />

      <script
        data-ad-client="ca-pub-3838260189413684"
        async
        src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
      />
    </Helmet>
  );
}

SEO.defaultProps = {
  googleMapsApiKey: null,
  lang: 'en',
  og: {},
};

SEO.propTypes = {
  googleMapsApiKey: PropTypes.string,
  title: PropTypes.string.isRequired,
  lang: PropTypes.string,
  meta: PropTypes.array.isRequired,
  og: PropTypes.object,
};

export default SEO;
