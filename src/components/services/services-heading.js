import React from 'react';
import { PropTypes } from 'prop-types';

const ServicesHeading = ({ tagline }) => (
  <div className="col-12">
    <strong className="font--48">{'SERVICES'}</strong>
    <br />
    <div dangerouslySetInnerHTML={{ __html: tagline }} />
    <br />
    <br />
    <br />
  </div>
);

ServicesHeading.propTypes = {
  tagline: PropTypes.string.isRequired,
};

export default ServicesHeading;
