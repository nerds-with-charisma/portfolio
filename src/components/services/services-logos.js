import React from 'react';
import { PropTypes } from 'prop-types';

const ServicesLogos = ({ logos }) => (
  <div className="col-12 col-lg-4">
    <br />
    <div className="row grid text-center">
      {logos.map((logo) => (
        <div className={`col-${logo.gridSize}`} key={logo.title}>
          <img
            className="serviceLogo"
            width="50"
            src={logo.url}
            alt={logo.title}
          />
        </div>
      ))}

      <button
        type="button"
        className="btn btn--gradient font--light font--21 radius--lg border--none no--wrap col-12"
        onClick={() =>
          document
            .getElementById('contact')
            .scrollIntoView({ behavior: 'smooth' })
        }
      >
        <strong>{'Need More Info? Reach Out!'}</strong>
      </button>
    </div>
  </div>
);

ServicesLogos.propTypes = {
  logos: PropTypes.array.isRequired,
};

export default ServicesLogos;
