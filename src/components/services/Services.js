import React from 'react';
import { PropTypes } from 'prop-types';
import LazyLoad from 'react-lazyload';

import LargeLetter from '../common/large-letter';
import ServicesHeading from './services-heading';
import ServicesSkills from './services-skills';
import ServicesLogos from './services-logos';

const Services = ({ servicesData }) => (
  <section
    id="services"
    className="container position--relative overflow--container"
  >
    <div className="grid text-center">
      <span className="opacity1">
        <LargeLetter letter={servicesData.letter} />
      </span>
      <ServicesHeading tagline={servicesData.tagline} />
      <ServicesSkills services={servicesData.services} />
      <LazyLoad offset={400}>
        <ServicesLogos logos={servicesData.logos} />
      </LazyLoad>
    </div>
  </section>
);

Services.propTypes = {
  servicesData: PropTypes.object.isRequired,
};

export default Services;
