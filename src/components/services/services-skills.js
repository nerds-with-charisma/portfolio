import React from 'react';
import { PropTypes } from 'prop-types';

const ServicesSkills = ({ services }) => (
  <>
    {services.map((service) => (
      <div key={service.title} className="col-12 col-md-6 col-lg-4 lh-md">
        <strong>{service.title}</strong>

        <hr className="shorty opacity5" />

        {service.items.map((item) => (
          <React.Fragment key={item}>
            {item}
            <br />
          </React.Fragment>
        ))}
      </div>
    ))}
  </>
);

ServicesSkills.propTypes = {
  services: PropTypes.array.isRequired,
};

export default ServicesSkills;
