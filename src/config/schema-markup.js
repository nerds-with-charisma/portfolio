/* eslint-disable */
const context = 'http://schema.org';
const email = 'briandausman@gmail.com';
const name = 'Brian Dausman';
const url = 'https://nerdswithcharisma.com';
const telephone = '815 403 4803';
const description =
  'Nerds With Charisma is a cutting edge digital media agency specializing in awesome websites.';

const address = {
  '@type': 'PostalAddress',
  addressLocality: 'Bartlett',
  addressRegion: 'IL',
  postalCode: '60103',
  streetAddress: '436 Smoketree Ln.',
};

const sameAs = [
  'https://twitter.com/nerdswcharisma',
  'https://www.facebook.com/Nerds-With-Charisma-728481650525727/',
  'https://gitlab.com/nerds-with-charisma',
  'https://www.instagram.com/ernie_hudsons_paycheck/',
];

const image = {
  '@type': 'ImageObject',
  url: 'https://nerdswithcharisma.com/nwc-tile.png',
  height: '1200',
  width: '1200',
};

const logo = {
  '@type': 'ImageObject',
  url: 'https://nerdswithcharisma.com/logo--nwc-purple.svg',
  height: '1200',
  width: '1200',
};

exports.data = [
  {
    '@type': 'Person',
    colleague: ['http://andrewbieganski.com/'],
    image: 'https://nerdswithcharisma.com/images/portfolio/brian.jpg',
    jobTitle: 'Developer',
    alumniOf: 'Northern Illinois University',
    gender: 'male',
    '@context': context,
    email: email,
    name: name,
    url: url,
    address: {
      ...address,
    },
  },
  {
    '@context': context,
    '@type': 'Corporation',
    name: name,
    url: url,
    sameAs: sameAs,
    image: image,
    telephone: telephone,
    email: email,
    address: {
      ...address,
    },
    logo: logo,
    location: {
      '@type': 'Place',
      name: name,
      telephone: telephone,
      image: image,
      logo: logo,
      url: 'https://nerdswithcharisma.com/',
      sameAs: sameAs,
      address: {
        ...address,
      },
    },
  },
  {
    '@context': context,
    '@type': 'WebSite',
    name: name,
    description: description,
    url: url,
    image: 'https://nerdswithcharisma.com/logo--nwc-purple.svg',
    sameAs: sameAs,
    copyrightHolder: {
      '@type': 'Corporation',
      name: name,
      url: url,
      sameAs: sameAs,
      image: image,
      telephone: telephone,
      email: email,
      address: {
        ...address,
      },
      logo: logo,
      location: {
        '@type': 'Place',
        name: name,
        telephone: telephone,
        image: image,
        logo: logo,
        url: url,
        sameAs: sameAs,
        address: {
          ...address,
        },
      },
    },
    author: {
      '@type': 'Corporation',
      name: name,
      url: url,
      sameAs: sameAs,
      image: image,
      telephone: telephone,
      email: email,
      address: {
        ...address,
      },
      logo: logo,
      location: {
        '@type': 'Place',
        name: name,
        telephone: telephone,
        image: image,
        logo: logo,
        url: url,
        sameAs: sameAs,
        address: {
          ...address,
        },
      },
    },
    creator: {
      '@type': 'Organization',
    },
  },
  {
    '@context': context,
    '@type': 'Place',
    name: name,
    telephone: telephone,
    image: image,
    logo: logo,
    url: url,
    sameAs: sameAs,
    address: {
      ...address,
    },
  },
  {
    '@context': context,
    '@type': 'BreadcrumbList',
    itemListElement: [
      {
        '@type': 'ListItem',
        position: '1',
        item: {
          '@id': url,
          name: 'Home',
        },
      },
    ],
  },
];
