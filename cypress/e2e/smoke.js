describe('app', () => {
  // we can get to the homepage
  it('initial load looks ok', () => {
    cy.visit('/');

    // todo: page title looks right
  });

  // nav tests
  it('nav looks ok', () => {
    // nav opens
    cy.visit('/')
    .get('#nav--btn')
    .click();

    // nav logo rendered
    cy.get('#nav > img').should('be.visible');

    // nav links are properly found
    cy.get('#nav > :nth-child(5) > strong').findByText(/home/i);
    cy.get('#nav > :nth-child(7) > strong').findByText(/services/i);
    cy.get('#nav > :nth-child(9) > strong').findByText(/portfolio/i);
    cy.get('#nav > :nth-child(11) > strong').findByText(/about/i);
    cy.get('#nav > :nth-child(13) > strong').findByText(/contact/i);
  });


  it('homepage looks good', () => {
    cy.visit('/')
    .contains('.font--125', 'websites.');
  });

  // todo: make sure all sections load
});