const path = require(`path`); // you will need it later to point at your template component

exports.createPages = async ({ graphql, boundActionCreators }) => {
  const { createPage } = boundActionCreators;

  // ## Create portfolio pages
  // we use a Promise to make sure the data are loaded
  // before attempting to create the pages with them
  const portfolioPromise = new Promise((resolve, reject) => {
    // fetch your data here, generally with graphQL.
    graphql(`
      {
        site {
          siteMetadata {
            title
            navigation
            homepageData {
              title
              lang
              meta {
                name
                content
              }
            }
            servicesData {
              letter
              tagline
              services {
                title
                items
              }
              logos {
                url
                gridSize
                title
              }
            }
            portfolioData {
              tagline
              portfolioData {
                alt
                src
                type
                website
                about
                images
                hero
                tech
                launchDate
                sort
                browser
              }
            }
          }
        }
      }
    `).then((result) => {
      if (result.errors) {
        // first check if there is no errors
        reject(result.errors); // reject Promise if error
      }

      // if no errors, you can map into the data and create your static pages
      result.data.site.siteMetadata.portfolioData.portfolioData.forEach(
        (node) => {
          // create page according to the fetched data
          createPage({
            path: `/project/${node.alt
              .toLowerCase()
              .replace(/ /g, '-')
              .toLowerCase()}`, // your url -> /categories/animals
            component: path.resolve('./src/pages/index.js'), // your template component
            context: {
              currentProjectToOpen: node,
            },
          });
        },
      );

      resolve();
    });
  });

  return {
    portfolioPromise,
  };
};
